<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Dcode – Saas & Software Landing Page Website Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="App and Saas Landing Template" />
        <meta name="keywords" content="Application, Clean, Saas, Dashboard, Bootstrap4, Software Landing, HTML5 Template" />
        <meta content="sacredthemes" name="author" />
        <!-- favicon -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <!-- WOW Animation -->
        <link href="/css/animate.css" rel="stylesheet" type="text/css"  />
        <!-- Bootstrap css -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Slick Slider -->
        <link href="/css/slick.css" rel="stylesheet" type="text/css"  />
        <!-- Icons -->
        <link href="/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/line-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/css/fontawesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Main css File -->
        <link href="/css/style.css" rel="stylesheet" type="text/css" id="theme-default" />
        <link href="/css/rtl-style.css" rel="stylesheet" type="text/css" id="rtl-theme-default" disabled="disabled" />
        <link href="/css/colors/default-color.css" rel="stylesheet" type="text/css" id="theme-color" />
    </head>
    <body>
        <!-- box-wrapper --> 
        <div class="box-wrapper">
            <!-- Loader -->
            <div id="preloader">
                <div id="status">
                    <div class="d-loader">
                        <img src="images/dcode-loader.gif" alt="DCode">
                    </div>
                </div>
            </div>
        </div>    
            <!-- Loader -->
            <!-- Nav Bar -->
            <header id="master-head" class="navbar menu-center">
                <div class="container-fluid">
                    <div id="main-logo" class="logo-container">
                        <a class="logo" href="/">
                            <img src="images/d-code-logo-dark.svg" class="logo-dark" alt="DCode">
                            <img src="images/d-code-logo-light.svg" class="logo-light" alt="DCode">
                        </a>
                    </div>
                    <div class="menu-toggle-btn">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle">
                            <div class="burger-lines">
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </div>
                    <div id="navigation" class="nav navbar-nav navbar-main">
                        <ul id="main-menu" class="menu-primary">
                            <li class="menu-item ">
                                <a href="index-standard.html">Home</a> 
                            </li>
                            <li class="menu-item menu-item-has-children active">
                                <a href="#">Služby</a>
                                <ul class="sub-menu">
                                    <li class="menu-item ">
                                        <a href="#">Váš Visap projekt</a>
                                        
                                    </li>
                                    <li class="menu-item">
                                        <a href="page-services.html">Visap family</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="page-career.html">Individuální řešení</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="page-job-details.html">Průmyslové řešení</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item">
                                <a href="#">Reference</a>
                            </li>
                            <li class="menu-item">
                                <a href="#">O nás</a>
                            </li>
                            <li class="menu-item">
                                <a href="#">Na počátku bylo světlo</a>
                            </li>
                            <li class="menu-item">
                                <a href="#" target="_blank">
                                <div class="btn btn-primary">KONTAKT</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>
            <!-- Nav Bar -->
            <!-- Main Wrapper -->
            <div id="main-wrapper" class="page-wrapper">
                    <!-- Page Header -->
                <div class="about-section ">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="heading-wrapper with-separator">
                                    <h2 class="h1">Vytváříme pracovní prostředí budoucnosti </h2>
                                </div><!-- End Heading -->
                                <div class="text-wrapper">
                                    <p>Pomáháme společnostem růst pomocí nejmodernějších technologií. Zlepšujeme jejich logistické cykly, distribuci, výrobu i lidské zdroje. Naším cílem je přicházet s řešeními, která podporují růst a šetří zdroje.
                                    </p>
                                </div>
                                <div class="btn-wrapper">
                                    <a class="btn btn-primary" href="#">KONTAKT</a>
                                    <a class="btn btn-outline-primary" href="#">O NÁS</a>
                                </div>
                            </div><!-- End Col -->
                            <div class="col-lg-6 wow fadeInLeft">
                                <div class="image-wrapper">
                                    <img src="images/default-color/user-interface-img.png" alt="">
                                </div>
                            </div><!-- End Col -->
                        </div><!-- End Row -->
                    </div>
                    <div class="light-bg section-padding">
                        <div class="container">
                            <div class="row clearfix justify-content-center">
                                <div class="col-lg-10">
                                    <div class="heading-wrapper text-center with-separator">
                                        <h2 class="h1">Naše <span>služby</span></h2>
                                            <div class="lead-text">
                                                <p>
                                                Jsme zkušeným dodavatelem inovativních řešení budoucnosti pro firmy v ČR i Evropě. Naši klienti pochází z různých oborů logistiky, průmyslu i dopravy.
                                                </p>
                                            </div>
                                    </div>
                                
                                </div>
                            </div>
                            <!-- End Row -->
                            <div class="row clearfix">
                            <div class="col-lg-4">
                                <div class="icon-box theme-two wow fadeInLeft">
                                    <div class="icon">
                                        <img src="images/default-color/icon-8.svg" alt="" />
                                    </div>
                                    <div class="text">
                                        <h4>
                                        Umělá inteligence v logistice 
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        <!-- End Col -->
                        <div class="col-lg-4">
                            <div class="icon-box theme-two wow fadeInUp">
                                <div class="icon">
                                    <img src="images/default-color/icon-9.svg" alt="" />
                                </div>
                                <div class="text">
                                    <h4>Monitoring pohybu věcí</h4>
                                </div>
                            </div>
                        </div>
                        <!-- End Col -->
                        <div class="col-lg-4">
                            <div class="icon-box theme-two wow fadeInRight">
                                <div class="icon">
                                    <img src="images/default-color/icon-10.svg" alt="" />
                                </div>
                                <div class="text">
                                    <h4>Logistika řízení a podpora</h4> 
                                </div>
                            </div>
                        </div>
                        <!-- End Col -->
                    </div>  
                    <div class="row clearfix mt-5">
                        <div class="col-lg-4 ml-auto">
                            <div class="icon-box theme-two wow fadeInLeft">
                                <div class="icon">
                                    <img src="images/default-color/icon-10.svg" alt="" />
                                </div>
                                <div class="text">
                                    <h4>Skladová logistika</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mr-auto">
                            <div class="icon-box theme-two wow fadeInRight">
                                <div class="icon">
                                    <img src="images/default-color/icon-10.svg" alt="" />
                                </div>
                                <div class="text">
                                    <h4>Bezpečná pracoviště budoucnosti</h4>
                                </div>
                            </div>
                        </div>
                    </div>         
                </div><!-- End Col -->
            </div><!-- End Row -->
            <div class="section-padding">
                <div class="container">
                    <div class="row clearfix pb-5">
                        <div class="col-lg-6 col-md-12 ">
                            <div class="heading-wrapper with-separator">
                                <h2 class="h1">Jak <span>pracujeme?</span></h2>
                            <div class="lead-text">
                                <p>Navrhujeme řešení podle skutečných potřeb našich zákazníků. </p>
                                <p>Nové spolupráce začínáme analýzou procesů a realizovaných use cases. Zefektivníme je a pomůžeme s jejich digitalizací. Za použití technologií, jako jsou virtuální reality a umělé inteligence zajistíme maximální efektivitu a minimalizujeme plýtvání zdrojů. </p>
                                <p class="mb-6">Vytvoříme pracovní prostředí budoucnosti.</p>
                            </div>
                        </div>
                        <div class="btn-wrapper">
                            <a class="btn btn-primary mt-5" href="#"><i class="fas fa-address-book"></i>DOMLUVIT SCHŮZKU</a>
                        </div>
                            <!-- End Heading -->
                    </div><!-- End Col -->
                                        <div class="col-lg-6 col-md-12">
                                            <div class="features-block theme-five wow fadeInLeft">
                                                <div class="inner-box">
                                                    <div class="icon">
                                                        <img class="normal" src="images/default-color/gradient-icon-1.svg" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <h4>Analýza</h4>    
                                                        <p>Díky celkové analýze vašich procesů najdeme prostor pro optimalizaci.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-block theme-five wow fadeInRight">
                                                <div class="inner-box">
                                                    <div class="icon">
                                                        <img class="normal" src="images/default-color/gradient-icon-2.svg" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <h4>Monetizace projektu</h4>    
                                                        <p>Představíme vám několik řešení spolu s kalkulacemi návratnosti.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-block theme-five wow fadeInLeft">
                                                <div class="inner-box">
                                                    <div class="icon">
                                                        <img class="normal" src="images/default-color/gradient-icon-3.svg" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <h4>Testování </h4>    
                                                        <p>Vyvineme testovací prostředí na základě vámi vybraného projektu.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-block theme-five wow fadeInRight">
                                                <div class="inner-box">
                                                    <div class="icon">
                                                        <img class="normal" src="images/default-color/gradient-icon-1.svg" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <h4>Návrh</h4>    
                                                        <p>Naše podpora se postará o bezchybné zapracování do provozu.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-block theme-five wow fadeInLeft">
                                                <div class="inner-box">
                                                    <div class="icon">
                                                        <img class="normal" src="images/default-color/gradient-icon-2.svg" alt="">
                                                    </div>
                                                    <div class="text">
                                                        <h4>Implementace</h4>    
                                                        <p>Naše podpora se postará o bezchybné zapracování do provozu.</p>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div> 
                            </div>

                <div class="section-padding style-dark dark-bg gradient-heading-bg">
                    <div class="container">
                        <div class="row clearfix justify-content-center">
                        <div class="col-lg-8">
                            <div class="heading-wrapper text-center">
                            <h2 class="h1">Proč spolupracovat s Visap?</h2>
                            <div class="lead-text">
                                <p>
                                Naším hlavním cílem je rozvoj vašeho podnikání. Nenabízíme pouhé prostředky, jak toho dosáhnout, pomůžeme vám zefektivnit celý business plán.
                                </p>
                            </div>
                            </div>
                            <!-- End Heading -->
                            <div class="empty-space-60 clearfix"></div>
                        </div>
                        <!-- End Col -->
                        </div>
                        <!-- End Row -->
                        <div class="row clearfix">
                        <div class="col-lg-4 col-md-6">
                            <div class="features-block theme-two wow fadeInUp">
                            <div class="inner-box">
                                <div class="text">
                                <h4>Nejmodernější technologie</h4>
                                <p>
                                Vyvíjíme špičkové technologie, které patří k těm nejlepším na světovém trhu. 
                                </p>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- End Col -->
                        <div class="col-lg-4 col-md-6">
                            <div
                            class="features-block theme-two wow fadeInUp"
                            data-wow-delay="0.2s"
                            >
                            <div class="inner-box">
                                <div class="text">
                                <h4>Vlastní know-how</h4>
                                <p>
                                Disponujeme vlastním vývojovým týmem a výrobou klíčových komponent. Naši dodavatelé splňují kvalitativní certifikace.
                                </p>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- End Col -->
                        <div class="col-lg-4 col-md-6">
                            <div
                            class="features-block theme-two wow fadeInUp"
                            data-wow-delay="0.4s"
                            >
                            <div class="inner-box">
                                <div class="text">
                                <h4>Pilotní projekt</h4>
                                <p>
                                Účinnost a přínos našich technologií si můžete otestovat v našem showroomu nebo přímo ve vašich prostorech.
                                </p>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- End Col -->
                        <div class="col-lg-4 col-md-6">
                            <div class="features-block theme-two wow fadeInUp">
                            <div class="inner-box">
                                <div class="text">
                                <h4>Individuální přístup</h4>
                                <p>
                                Přizpůsobíme se vašim potřebám. Řešení napojíme na stávající systémy, které využíváte a vyvineme jej buď ve vašem, nebo našem prostředí.

                                </p>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- End Col -->
                        <div class="col-lg-4 col-md-6">
                            <div
                            class="features-block theme-two wow fadeInUp"
                            data-wow-delay="0.2s"
                            >
                            <div class="inner-box">
                                <div class="text">
                                <h4>Okamžitý start</h4>
                                <p>
                                S námi můžete začít inovovat už zítra. Většinu produktů a řešení držíme pro okamžité nasazení.

                                </p>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- End Col -->
                        <div class="col-lg-4 col-md-6">
                            <div
                            class="features-block theme-two wow fadeInUp"
                            data-wow-delay="0.4s"
                            >
                            <div class="inner-box">
                                <div class="text">
                                <h4>Spokojenost klientů</h4>
                                <p>
                                Dbáme na návratnost našich řešení. Naše podpora je vám plně k dispozici během vývoje i nasazení nového řešení.
                                </p>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!-- End Col -->
                        </div>
                        <!-- End Row -->
                    </div>
                    </div>
               
                                </div>
                            </div><!-- End Col -->
                        </div><!-- End Row -->
                    </div>
                </div>
                <!-- Testimonial Section -->
            </div>
            <!-- Main Wrapper -->
            <footer class="site-footer footer-theme-two">
                <div class="copyright-bar style-dark">
                    <div class="copyright-text text-center">
                        © Copyright Visap 2021. Created by <a href="https://creativehandles.com/" target="_blank" ><strong>Creative handles</strong></a>.
                    </div>
                </div> 
            </footer>

        <!-- javascript -->
        <script src="/js/jquery.min.js"></script>
        <script src="/js/jquery-migrate.min.js"></script>
        <script src="/js/bootstrap.bundle.min.js"></script>
        <script src="/js/jquery.easing.min.js"></script>
        <script src="/js/scrollspy.min.js"></script>
        <script src="/js/appear.js"></script>
        <!-- WOW Animation -->
        <script src="/js/wow.min.js"></script>
        <!-- Slick Slider -->
        <script src="/js/slick.min.js"></script>
        <!-- Main Js -->
        <script src="/js/dcode.js"></script>
    </body>
</html>
